const riderJson = [
  {"id": "0", "name": "The Fool"},
  {"id": "1", "name": "The Magician"},
  {"id": "2", "name": "The High Priestess"},
  {"id": "3", "name": "The Empress"},
  {"id": "4", "name": "The Emperor"},
  {"id": "5", "name": "The Hierophant"},
  {"id": "6", "name": "The Lovers"},
  {"id": "7", "name": "The Chariot"},
  {"id": "8", "name": "Strength"},
  {"id": "9", "name": "The Hermit"},
  {"id": "10", "name": "The Wheel of Fortune"},
  {"id": "11", "name": "Justice"},
  {"id": "12", "name": "The Hanged Man"},
  {"id": "13", "name": "Death"},
  {"id": "14", "name": "Temperance"},
  {"id": "15", "name": "The Devil"},
  {"id": "<t_co>", "name": "The Blasted Tower"},
  {"id": "17", "name": "The Star"},
  {"id": "18", "name": "The Moon"},
  {"id": "19", "name": "The Sun"},
  {"id": "20", "name": "Judgement"},
  {"id": "21", "name": "The World"},
  {"id": "22", "name": "King of Wands"},
  {"id": "23", "name": "Queen of Wands"},
  {"id": "24", "name": "Knight of Wands"},
  {"id": "25", "name": "Page of Wands"},
  {"id": "26", "name": "Ace of Wands"},
  {"id": "27", "name": "Two of Wands"},
  {"id": "28", "name": "Three of Wands"},
  {"id": "29", "name": "Four of Wands"},
  {"id": "30", "name": "Five of Wands"},
  {"id": "31", "name": "Six of Wands"},
  {"id": "32", "name": "Seven of Wands"},
  {"id": "33", "name": "Eight of Wands"},
  {"id": "34", "name": "Nine of Wands"},
  {"id": "35", "name": "Ten of Wands"},
  {"id": "36", "name": "King of Cups"},
  {"id": "37", "name": "Queen of Cups"},
  {"id": "38", "name": "Knight of Cups"},
  {"id": "39", "name": "Page of Cups"},
  {"id": "40", "name": "Ace of Cups"},
  {"id": "41", "name": "Two of Cups"},
  {"id": "42", "name": "Three of Cups"},
  {"id": "43", "name": "Four of Cups"},
  {"id": "44", "name": "Five of Cups"},
  {"id": "45", "name": "Six of Cups"},
  {"id": "46", "name": "Seven of Cups"},
  {"id": "47", "name": "Eight of Cups"},
  {"id": "48", "name": "Nine of Cups"},
  {"id": "49", "name": "Ten of Cups"},
  {"id": "50", "name": "King of Swords"},
  {"id": "51", "name": "Queen of Swords"},
  {"id": "52", "name": "Knight of Swords"},
  {"id": "53", "name": "Page of Swords"},
  {"id": "54", "name": "Ace of Swords"},
  {"id": "55", "name": "Two of Swords"},
  {"id": "56", "name": "Three of Swords"},
  {"id": "57", "name": "Four of Swords"},
  {"id": "58", "name": "Five of Swords"},
  {"id": "59", "name": "Six of Swords"},
  {"id": "60", "name": "Seven of Swords"},
  {"id": "61", "name": "Eight of Swords"},
  {"id": "62", "name": "Nine of Swords"},
  {"id": "63", "name": "Ten of Swords"},
  {"id": "64", "name": "King of Pentacles"},
  {"id": "65", "name": "Queen of Pentacles"},
  {"id": "66", "name": "Knight of Pentacles"},
  {"id": "67", "name": "Page of Pentacles"},
  {"id": "68", "name": "Ace of Pentacles"},
  {"id": "69", "name": "Two of Pentacles"},
  {"id": "70", "name": "Three of Pentacles"},
  {"id": "71", "name": "Four of Pentacles"},
  {"id": "72", "name": "Five of Pentacles"},
  {"id": "73", "name": "Six of Pentacles"},
  {"id": "74", "name": "Seven of Pentacles"},
  {"id": "75", "name": "Eight of Pentacles"},
  {"id": "76", "name": "Nine of Pentacles"},
  {"id": "77", "name": "Ten of Pentacles"}
];

const kingWenJson = [
  {"id": "0", "name": "Hexagram for the Creative Heaven"},
  {"id": "1", "name": "Hexagram for the Receptive Earth"},
  {"id": "2", "name": "Hexagram for Difficult at the Beginning"},
  {"id": "3", "name": "Hexagram for Youthful Folly"},
  {"id": "4", "name": "Hexagram for Waiting"},
  {"id": "5", "name": "Hexagram for Conflict"},
  {"id": "6", "name": "Hexagram for the Army"},
  {"id": "7", "name": "Hexagram for Holding Together"},
  {"id": "8", "name": "Hexagram for Small Taming"},
  {"id": "9", "name": "Hexagram for Treading"},
  {"id": "10", "name": "Hexagram for Peace"},
  {"id": "11", "name": "Hexagram for Standstill"},
  {"id": "12", "name": "Hexagram for Fellowship"},
  {"id": "13", "name": "Hexagram for Great Possession"},
  {"id": "14", "name": "Hexagram for Modesty"},
  {"id": "15", "name": "Hexagram for Enthusiasm"},
  {"id": "<t_co>", "name": "Hexagram for Following"},
  {"id": "17", "name": "Hexagram for Work on the Decayed"},
  {"id": "18", "name": "Hexagram for Approach"},
  {"id": "19", "name": "Hexagram for Contemplation"},
  {"id": "20", "name": "Hexagram for Biting Through"},
  {"id": "21", "name": "Hexagram for Grace"},
  {"id": "22", "name": "Hexagram for Splitting Apart"},
  {"id": "23", "name": "Hexagram for Return"},
  {"id": "24", "name": "Hexagram for Innocence"},
  {"id": "25", "name": "Hexagram for Great Taming"},
  {"id": "26", "name": "Hexagram for Mouth Corners"},
  {"id": "27", "name": "Hexagram for Great Preponderance"},
  {"id": "28", "name": "Hexagram for the Abysmal Water"},
  {"id": "29", "name": "Hexagram for the Clinging Fire"},
  {"id": "30", "name": "Hexagram for Influence"},
  {"id": "31", "name": "Hexagram for Duration"},
  {"id": "32", "name": "Hexagram for Retreat"},
  {"id": "33", "name": "Hexagram for Great Power"},
  {"id": "34", "name": "Hexagram for Progress"},
  {"id": "35", "name": "Hexagram for Darkening of the Light"},
  {"id": "36", "name": "Hexagram for the Family"},
  {"id": "37", "name": "Hexagram for Opposition"},
  {"id": "38", "name": "Hexagram for Obstruction"},
  {"id": "39", "name": "Hexagram for Deliverance"},
  {"id": "40", "name": "Hexagram for Decrease"},
  {"id": "41", "name": "Hexagram for Increase"},
  {"id": "42", "name": "Hexagram for Breakthrough"},
  {"id": "43", "name": "Hexagram for Coming to Meet"},
  {"id": "44", "name": "Hexagram for Gathering Together"},
  {"id": "45", "name": "Hexagram for Pushing Upward"},
  {"id": "46", "name": "Hexagram for Oppression"},
  {"id": "47", "name": "Hexagram for the Well"},
  {"id": "48", "name": "Hexagram for Revolution"},
  {"id": "49", "name": "Hexagram for the Cauldron"},
  {"id": "50", "name": "Hexagram for the Arousing Thunder"},
  {"id": "51", "name": "Hexagram for the Keeping Still Mountain"},
  {"id": "52", "name": "Hexagram for Development"},
  {"id": "53", "name": "Hexagram for the Marrying Maiden"},
  {"id": "54", "name": "Hexagram for Abundance"},
  {"id": "55", "name": "Hexagram for the Wanderer"},
  {"id": "56", "name": "Hexagram for the Gentle Wind"},
  {"id": "57", "name": "Hexagram for the Joyous Lake"},
  {"id": "58", "name": "Hexagram for Dispersion"},
  {"id": "59", "name": "Hexagram for Limitation"},
  {"id": "60", "name": "Hexagram for Inner Truth"},
  {"id": "61", "name": "Hexagram for Small Preponderance"},
  {"id": "62", "name": "Hexagram for After Completion"},
  {"id": "63", "name": "Hexagram for Before Completion"}
];

const oneCardJson = [
  { "id": "0", "name": "fortune"}
];

const threeCardJson = [
  {"id": "0", "name": "past"},
  {"id": "1", "name": "present"},
  {"id": "2", "name": "future"}
];

const iChingingJson = [
  { "id": "0", "name": "base"},
  { "id": "1", "name": "transform"}
];

const layoutTypes = [
  {id: 0, name: "oneCard", items: 1, type: "tarot", json: oneCardJson},
  {id: 1, name: "threeCard", items: 3, type: "tarot", json: threeCardJson},
  {id: 2, name: "iChinging", items: 2, type: "iChing", json: iChingingJson}
]
// We should probably add a key / value like:
// propername: "One Card"
// for when this needs to be displayed.

const divinationTypes = [
  {id: 0, name: "rider", items: 78, type: "tarot", json: riderJson},
  {id: 1, name: "kingWen", items: 3, type: "iChing", json: kingWenJson}
];
// We should probably add a key / value like:
// propername: "King Wen"
// for when this needs to be displayed.
// Also, we might want to add something like:
// indextype : which would be either 0 or 1
// depending on if they start at 0 or 1.
// If not here, then we'll want to figure out
// some way to do it. At least for iChing
// where Hexagrams start at 1.
// indextype seems like a cheaper idea then
// adding an additional field like number
// and pulling that for each item, as it will
// usually be redundant.

class Stack {
  constructor(divinationObject) {
    this.stack = [];
    this.divination = this.initialize(divinationObject);
    // This will be different when we merge initializeLayout()
  }
  initialize(divinationObject) {
    return divinationTypes.find(item => item.name === divinationObject);
  }
  // Modify this when we merge initializeLayout()
}

class Table extends Stack {
  constructor(divinationObject,layoutPattern) {
    // I modified arguments here. Nothing
    // needs to be changed, just pointing it out.
    super(divinationObject);
    this.stackDB = this.divination["json"];
    // The above lines will also be different when we merge
    // initalizeLayout()
    this.layout = this.stack;
    this.layoutOptions = this.initializeLayout(layoutPattern);
    // I modified the way this is called.
    // Just pointing that out.
    // Later on we will ...
    // Modify when we merge initializeLayout()
    this.layoutDB = this.layoutOptions["json"];
  }
  cartomancy() {
    this.pattern = [];
    this.spread = [];
    for(let i = 0; i < this.layoutOptions["items"]; i++) {
      this.pattern.push(this.lookupLayout(i));
      this.spread.push(this.lookupCard(this.layout[i]));
      // Get rid of this.pattern
      // this.spread will be an array object
      // each part of the array will have two keys:
      // card and position
      // This should not push, as we don't want to remove the cards.
      // Instead in each iteration of the loop
      // you should add the card name with lookupCard
      // and the position name with lookupLayout
    }
    document.write(this.pattern);
    document.write(this.spread);
    // instead of writing, just return this.spread
  }
  lookupCard(tarotCard) {
    return this.stackDB.find(item => item.id == tarotCard).name;
  }
  lookupLayout(layoutPosition) {
    return this.layoutDB.find(item => item.id == layoutPosition).name;
  }
  // the two lookup functions are practically the same
  // we should make this just one lookup function
  // it will take two arguments:
  // lookup(lookupDB,item)
  // Examples of how this would be called:
  // lookup(this.stackDB,this.layout[i])
  // or
  // lookup(this.layoutDB,i)
  // Those are rewrites of what is in Cartomancy
  // Also,
  // We probably want to move lookout into Stack
  initializeLayout(layoutType) {
    return layoutTypes.find(item => item.name === layoutType);
  }
  // This should be merged with initialize() in Stack
  // We will talk about how we are going to do this.
}

class Deck extends Stack {
  constructor(divinationObject) {
    super(divinationObject);
    this.deck = this.stack;
    // I think this line can be removed.
    // Then in createDeck()
    // we just call:
    // this.deck = this.stack;
    // Since this.stack always equals []
    // It provided a clean initialization each time.
    this.createDeck(this.divination["items"]);
    // Modify when we merge initalizeLayout()
  }
  shuffle() {
    let i = this.deck.length, j;
    while (i) {
      j = Math.floor(Math.random() * i--);
      [this.deck[i], this.deck[j]] = [this.deck[j], this.deck[i]];
    }
  }
  createDeck(cards) {
    this.deck = [];
    // See note in constructor()
    // for how we should go about changing this.
    // I tested it and it seemed to work out fine.
    while (cards--) {
      this.deck.push(cards);
    }
    this.shuffle();
  }
  deal(hand) {
    return this.deck.splice(0,hand);
  }
}

const method = new Deck("rider");
// This is temporary, we should
// rename this, and othe variables/
// classes/functions to make most
// sense.
const table = new Table("rider","threeCard");
table.layout = method.deal(3);
table.cartomancy();
// cartonmancy is going to return an object
// so wrap this in a document.write
// or even a console.log for now
