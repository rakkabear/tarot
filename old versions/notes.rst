(Quick cheatsheet for rst)
Conventions for Part Underlining:

# with overline, for parts
* with overline, for chapters
=, for sections
-, for subsections
^, for subsubsections
“, for paragraphs

##############
The First Part
##############

These are the notes for this project. Not sure what to put here first but for now I'm going to dive right in...

*******************
Setup - Environment
*******************

I decided to use JavaScript because I'm doing the fullstack developer thing, and JavaScript has a bunch of exciting new server-side possibilities that I want to explore. I'm already a little familiar with Python and I wanted to try this whole 'isometric application developemnt' the kids are all talking about these days. 

To start at the very beginning, I'm going to set up a basic dev environment with node.js, npm, and express as a dev server. Key word here is 'basic' - read 'simple'. For the moment, I just want the ability to whip out some code, test it, lint it, and watch it for changes so I don't have to keep kicking up a server over and over. 

For a text editor I'm using VSCode and it's pretty nice. I could go into more detail about everything else I'm using (operating system? hardware?) but I'll hold off on that for now. 

Node Setup
==========
Assuming you've grabbed the most recent (stable) versions of node.js and with it, npm, make a directory for your project, cd into it, and start up ```npm init``` and add your basic info. 


Resources for this entry: 
http://corgibytes.com/blog/2016/09/27/minimal-useful-javascript-environment/

https://dev.to/aurelkurtula/setting-up-a-minimal-node-environment-with-webpack-and-babel--1j04

Also this pluralsight program is a little dated but still handy: https://www.pluralsight.com/courses/javascript-development-environment

I want to read this before I make this setup more complicated in the future: https://www.contentful.com/blog/2017/04/04/es6-modules-support-lands-in-browsers-is-it-time-to-rethink-bundling/ 