This is the beginning of an app I'm making that will allow you to play with tarot cards and the iChing to tell your fortune! 

Eventually I plan to expand it to include other divination types, but this is the bare-bones, MVP of the app right now. 

Next steps are to get the data into a database, get a backend with either Python or node.js, and refactor the front end to use React. 